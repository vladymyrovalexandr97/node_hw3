import {Router} from 'express';
import authorithationMiddleware from './authorithationMiddleware.js';
import userController from './userController.js';

const userRouter = new Router();

userRouter.get('/me', authorithationMiddleware, userController.getUser);
userRouter.patch('/me', authorithationMiddleware, userController.updateUser);
userRouter.delete('/me', authorithationMiddleware, userController.deleteUser);

export default userRouter;
