import {Router} from 'express';
import authorithationMiddleware from './authorithationMiddleware.js';
import truckContoller from './truckController.js';

const truckRouter = new Router();

/* eslint-disable max-len*/
truckRouter.get('', authorithationMiddleware, truckContoller.getUserTruck);
truckRouter.post('', authorithationMiddleware, truckContoller.addTruckForUser);
truckRouter.get('/:id', authorithationMiddleware, truckContoller.getUserTruckById);
truckRouter.put('/:id', authorithationMiddleware, truckContoller.updateUserTruckById);
truckRouter.delete('/:id', authorithationMiddleware, truckContoller.deleteUserTruckById);
truckRouter.post('/:id/assign', authorithationMiddleware, truckContoller.assignTruckById);

export default truckRouter;
