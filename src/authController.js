import userModel from './models/userModel.js';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

/* eslint-disable no-undef */
const generateAccessToken = (id, email, role) => {
  const payload = {
    id,
    email,
    role,
  };
  return jwt.sign(payload, process.env.SECRET_TOKEN, {expiresIn: '2h'});
};

/* eslint-disable new-cap */
class authController {
  async registration(req, res) {
    try {
      const {email, password, role} = req.body;
      const candidate = await userModel.findOne({email});
      if (candidate) {
        throw new Error('User already exist');
      }
      const hashPassword = bcrypt.hashSync(password, 6);
      const user = new userModel(
          {
            role,
            email,
            password: hashPassword,
            createdDate: new Date().toISOString(),
          },
      );
      await user.save();
      return res.json({message: 'Profile created successfully'});
    } catch (e) {
      console.log(e);
      res.status(400).json({message: e.message});
    }
  }
  async login(req, res) {
    try {
      const {email, password} = req.body;
      const user = await userModel.findOne({email});
      if (!user) {
        res.status(400).json({message: `User doesnt exist`});
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        res.status(400).json({message: `Incorrect password`});
      }
      const token = generateAccessToken(user._id, user.email, user.role);
      return res.status(200).json({
        message: 'Success',
        jwt_token: token,
      });
    } catch (e) {
      res.status(400).json({message: 'Login error'});
    }
  }
}

export default new authController();
