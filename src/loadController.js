import loadModel from './models/loadModel.js';
import userModel from './models/userModel.js';

/* eslint-disable camelcase*/
/* eslint-disable no-empty*/
/* eslint-disable new-cap*/
class Loadcontroller {
  async getLoads(req, res) {
    try {
      const findUser = await userModel.findById(req.user.email);
      const {status, limit, offset} = req.query;
      if (findUser.role === 'SHIPPER') {
        const loads = await loadModel.find({created_by: req.user.email})
            .limit(+limit)
            .skip(+offset - 1);
        const newLoads = status ?
          loads.filter((item) => item.status === status) :
          loads;
        if (!loads) {
          res.status(400).json({message: 'Error'});
        }
        res.json({
          loads: newLoads,
        });
      }
    } catch (e) {
      console.log(e);
      res.status(400).json({message: e.message});
    }
  }
  async createLoad(req, res) {
    try {
      const {
        name, payload, pickup_address, delivery_address, dimensions,
      } = req.body;

      const load = new loadModel({
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
        created_date: new Date().toISOString(),
        created_by: req.user._id,
        status: 'NEW',
        logs: [{
          time: new Date().toISOString(),
          message: 'Load has been created',
        }],
      });

      await load.save();
      return res.json({message: 'Load created successfully'});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
  async getUserActiveLoad(req, res) {
    try {
      const load = await loadModel.findOne({assigned_to: req.user._id});
      return res.json({load});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
  async iterateToNextState(req, res) {
    try {
      const load = await loadModel.findOne({assigned_to: req.user._id});
      const newState = getNextState(load.state);
      load.state = newState;
      await load.save();
      return res.json({'message': `Load state changed to '${newState}'`});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
  async getLoadById(req, res) {
    try {
      let load = await loadModel.findById(req.params.id);
      if (load?.created_by !== req.user._id) {
        load = null;
      }
      return res.json({load});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
  async updateUserLoadById(req, res) {
    try {
      const load = await loadModel.findById(req.params.id);
      if (load?.created_by !== req.user._id) {
        throw new Error();
      }
      const {
        name, payload, pickup_address, delivery_address, dimensions,
      } = req.body;
      load.name = name;
      load.payload = payload;
      load.pickup_address = pickup_address;
      load.delivery_address = delivery_address;
      load.dimensions= dimensions;
      await load.save();
      return res.json({'message': 'Load details changed successfully'});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
  async deleteUserLoadById(req, res) {
    try {
      const load = await loadModel.findById(req.params.id);
      if (load?.created_by !== req.user._id) {
        throw new Error();
      }
      await load.deleteOne();
      return res.json({'message': 'Load deleted successfully'});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
  async postUserLoadById(req, res) {
    try {
      const load = await loadModel.findById(req.params.id);
      if (load?.created_by !== req.user._id) {
        throw new Error();
      }
      return res.json({
        'message': 'Load posted successfully',
        'driver_found': true,
      });
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
  async getShippingInfo(req, res) {
    try {
      const load = await loadModel.findById(req.params.id);
      if (load?.created_by !== req.user._id) {
        throw new Error();
      }
      return res.json({load});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
}

function getNextState(state) {
  if (state === 'En route to Pick Up') {
    return 'Arrived to pick up';
  } else if (state === 'Arrived to pick up') {
    return 'En route to delivery';
  } else if (state === 'En route to delivery') {
    return 'Arrived to delivery';
  }
  throw new Error('Next state do not exist');
}


export default new Loadcontroller();
