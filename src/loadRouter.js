import {Router} from 'express';
import authorithationMiddleware from './authorithationMiddleware.js';
import loadController from './loadController.js';

const loadRouter = new Router();

/* eslint-disable max-len*/
loadRouter.get('', authorithationMiddleware, loadController.getLoads);
loadRouter.post('', authorithationMiddleware, loadController.createLoad);
loadRouter.get('/active', authorithationMiddleware, loadController.getUserActiveLoad);
loadRouter.patch('/active/state', authorithationMiddleware, loadController.iterateToNextState);
loadRouter.get('/:id', authorithationMiddleware, loadController.getLoadById);
loadRouter.put('/:id', authorithationMiddleware, loadController.updateUserLoadById);
loadRouter.delete('/:id', authorithationMiddleware, loadController.deleteUserLoadById);
loadRouter.post('/:id/post', authorithationMiddleware, loadController.postUserLoadById);
loadRouter.get('/:id/shipping_info', authorithationMiddleware, loadController.getShippingInfo);

export default loadRouter;
