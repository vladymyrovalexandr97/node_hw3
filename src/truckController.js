import truckModel from './models/truckModel.js';

/* eslint-disable new-cap*/
class Truckcontoller {
  async getUserTruck(req, res) {
    try {
      const trucks = await truckModel.find({
        created_by: req.user._id,
      });
      return res.json({trucks});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
  async addTruckForUser(req, res) {
    try {
      const {type} = req.body;
      const truck = new truckModel({
        type,
        created_date: new Date().toISOString(),
        created_by: req.user._id,
        status: 'IS',
      });
      await truck.save();
      return res.json({message: 'Truck created successfully'});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: 'Truck creation error'});
    }
  }
  async getUserTruckById(req, res) {
    try {
      const id = req.params.id;
      let truck = await truckModel.findById(id);

      if (truck?.created_by !== req.user._id) {
        truck = null;
      }

      return res.json({truck});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
  async updateUserTruckById(req, res) {
    try {
      const id = req.params.id;
      const truck = await truckModel.findById(id);

      if (truck?.created_by !== req.user._id) {
        throw new Error();
      }

      truck.type = req.body.type;
      await truck.save();
      return res.json({'message': 'Truck details changed successfully'});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
  async deleteUserTruckById(req, res) {
    try {
      const id = req.params.id;
      const truck = await truckModel.findById(id);

      if (truck?.created_by !== req.user._id) {
        throw new Error();
      }

      await truck.deleteOne();

      return res.json({'message': 'Truck deleted successfully'});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
  async assignTruckById(req, res) {
    try {
      const id = req.params.id;
      const truck = await truckModel.findById(id);

      if (truck?.created_by !== req.user._id) {
        throw new Error();
      }

      truck.assigned_to = req.user._id;
      await truck.save();

      return res.json({'message': 'Truck assigned successfully'});
    } catch (err) {
      console.log(err);
      res.status(400).json({message: err.message});
    }
  }
}

export default new Truckcontoller();
