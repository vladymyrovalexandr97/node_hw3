import mongoose from 'mongoose';

const dimensionsSchema = new mongoose.Schema({
  width: {
    type: Number,
    required: true,
  },
  height: {
    type: Number,
    required: true,
  },
  length: {
    type: Number,
    required: true,
  },
});
const logSchema = new mongoose.Schema({
  message: {
    type: String,
    required: true,
  },
  time: {
    type: String,
    required: true,
  },
});

const loadSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String,
    required: true,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  },
  state: {
    type: String,
    enum: [
      'En route to Pick Up',
      'Arrived to pick up', 'En route to delivery', 'Arrived to delivery'],
  },
  created_date: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: dimensionsSchema,
    required: true,
  },
  logs: {
    type: [logSchema],
    required: true,
  },
});

export default mongoose.model('loads', loadSchema);
