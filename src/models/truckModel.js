import mongoose from 'mongoose';

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    required: true,
    enum: ['OL', 'IS'],
  },
  created_date: {
    type: String,
    required: true,
  },
});

export default mongoose.model('trucks', truckSchema);
